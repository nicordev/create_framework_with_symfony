<?php declare(strict_types=1);

$name = $request->query->get('name') ?? '';

?>

Hello <?= htmlspecialchars($name, ENT_QUOTES, 'UTF-8') ?>
