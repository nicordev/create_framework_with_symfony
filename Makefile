serve:
	symfony server:start --port=4321 --passthru=front.php

test:
	php ./vendor/bin/phpunit tests ${args}
