<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class NotFoundTest extends TestCase
{
    public function testNotFound(): void
    {
        $_SERVER['REQUEST_URI'] = '/unknown';

        ob_start();
        include __DIR__.'/../../web/front.php';
        $content = ob_get_clean();

        self::assertSame('Not Found.', trim($content), 'missing not found.');
    }
}
