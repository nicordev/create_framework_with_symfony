<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class HelloTest extends TestCase
{
    public function testHello(): void
    {
        $_SERVER['REQUEST_URI'] = '/hello';
        $_GET['name'] = 'nico';

        ob_start();
        include __DIR__.'/../../web/front.php';
        $content = ob_get_clean();

        self::assertSame('Hello nico', trim($content), 'wrong content');
    }

    public function testHelloWithXss(): void
    {
        $_SERVER['REQUEST_URI'] = '/hello';
        $_GET['name'] = '<script>alert("nico");</script>';

        ob_start();
        include __DIR__.'/../../web/front.php';
        $content = ob_get_clean();

        self::assertSame('Hello &lt;script&gt;alert(&quot;nico&quot;);&lt;/script&gt;', trim($content), 'wrong content');
    }
}
