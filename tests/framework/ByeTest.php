<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class ByeTest extends TestCase
{
    public function testHello(): void
    {
        $_SERVER['REQUEST_URI'] = '/bye';

        ob_start();
        include __DIR__.'/../../web/front.php';
        $content = ob_get_clean();

        self::assertSame('Goodbye!', trim($content), 'wrong content');
    }
}
